import numpy as np
import random as rand
from scipy.stats import levy_stable as levy

class Cuckoo:
    def __init__(self, value):
        self._value = value
        self._fitval = None
        self.fitness()

    "override this"
    def fitness(self):
        raise NameError("Not Implemented Yet. You have to implement this")

    "override this"
    def is_goal(self):
        raise NameError("Not Implemented Yet. You have to implement this")

    def set_value(self, value):
        self._value = value
        self.fitness()

    def get_fitval(self):
        return self._fitval

    def get_value(self):
        return self._value

    def __str__(self):
        info = "actual value : " + str(self.get_value())
        info += " fitness: " + str(self.get_fitval())
        return info


class SpaceSearch:
    def __init__(self, upper_bound, lower_bound, num_dimension, step_scale, factory_cuckoo, Lambda=1.5, beta=0):
        self.upper_bound = upper_bound
        self.lower_bound = lower_bound
        self.num_dimension = num_dimension
        self.step_scale = step_scale
        self.Lambda = Lambda
        self.factory_cuckoo = factory_cuckoo
        self.beta = beta

    def generate_random_cuckoo(self):
        value = np.random.uniform(self.lower_bound, self.upper_bound, self.num_dimension)
        return self.factory_cuckoo.create_cuckoo(value)

    def generate_via_lf(self, cuckoo):
        return cuckoo.get_value() + self.step_scale * levy.rvs(self.Lambda, self.beta, size=self.num_dimension)

    def levy_flight(self, cuckoo):
        new_position = self.generate_via_lf(cuckoo)
        is_on_low_bound = self.lower_bound < new_position
        is_down_up_bound = self.upper_bound > new_position

        if is_down_up_bound.all() and is_on_low_bound.all():
            return self.factory_cuckoo.create_cuckoo(new_position)
        return cuckoo


class Seeker:
    def __init__(self, population_size, discover_factor, max_generation, space_search):
        self.population_size = population_size
        self.discover_factor = discover_factor
        self.max_generation = max_generation
        self.spacesearch = space_search
        self.population = self.generate_population()
        self.__done = False

    def search(self, data_output={}):
        sortting_criterion = lambda cuckoo: cuckoo.get_fitval()
        self.population.sort(key=sortting_criterion)
        best_cuckoo = self.population[0]
        iteration = 0
        data_output["last_change"] = 0
        while iteration < self.max_generation and not best_cuckoo.is_goal():
            new_cuckoo = self.spacesearch.levy_flight(best_cuckoo)
            chosen_cuckoo = rand.choice(self.population)
            if new_cuckoo.get_fitval() < chosen_cuckoo.get_fitval():
                chosen_cuckoo.set_value(new_cuckoo.get_value())
            self.replace_worsts(self.population)
            self.population.sort(key=sortting_criterion)
            best_cuckoo = self.population[0]
            iteration += 1
            self.update_data_output(data_output, iteration, best_cuckoo)
            print("Iteration: " + str(iteration) + str(best_cuckoo) + " last change: "+ str(data_output["last_change"]))
        data_output["done"] = True
        self.__done = True
        return best_cuckoo

    def replace_worsts(self, population):
        fraction = int(round(self.population_size * self.discover_factor))
        population = population[:fraction]
        bests = population.copy()
        for i in range(fraction):
            chosen_cuckoo = rand.choice(bests)
            new_cuckoo = self.spacesearch.levy_flight(chosen_cuckoo)
            population.append(new_cuckoo)

    def generate_population(self):
        population = [self.spacesearch.generate_random_cuckoo() for _ in range(self.population_size)]
        return population

    def update_data_output(self, data, iteration, best):
        if data is not None:
            data["last_change"] += 1
            if iteration > 1:
                if data["best"]._fitval != best._fitval:
                    data["last_change"] = 0

            data["best"] = best
            data["iteration"] = iteration
            data["done"] = False



    def is_done(self):
        return self.__done


