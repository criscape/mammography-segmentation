from src.Cuckoo import Cuckoo, SpaceSearch, Seeker
import numpy as np
from src.ga_utils import fit

UPPER_LIMIT = 128
LOWER_LIMIT = 0
THRESHOLDS = 16
STEP_SCALE = 1
ALPHA = 1.5
POPULATION_SIZE = 80
GENERATIONS = 100
DISCOVER_FACTOR = 0.3

class CuckooThreshold(Cuckoo):
    hist = None
    image_size = None

    def __init__(self, value):
        super().__init__(value)

    def fitness(self):
        ks = []
        last_threshold = 0
        for current_threshold in self.get_value():
            ks.append([i for i in range(last_threshold, current_threshold)])
            last_threshold = current_threshold
        self._fitval = fit(ks, 1, self.hist, self.image_size)

    def is_goal(self):
        return False

class ThresholdSpaceSearch(SpaceSearch):
    def __init__(self, upper_bound, lower_bound, num_dimension, step_scale, factory_cuckoo, Lambda=1.5, beta=0):
        super().__init__(upper_bound,lower_bound,num_dimension,step_scale,factory_cuckoo,Lambda, beta)

    def generate_random_cuckoo(self):
        value = np.zeros(self.num_dimension, dtype=int)
        space = int(round(self.upper_bound/self.num_dimension))
        for i in range(self.num_dimension):
            min_value = i * space
            max_value = min_value + space - 1
            value[i] = np.random.random_integers(min_value, max_value)
        return self.factory_cuckoo.create_cuckoo(value)

    def levy_flight(self, cuckoo):
        new_position = self.generate_via_lf(cuckoo)
        new_position = np.around(new_position).astype(int)

        is_on_space = True
        for i in range(1, len(new_position)-1):
            is_on_space = is_on_space and new_position[i-1] < new_position[i] < new_position[i+1]

        if is_on_space:
            return self.factory_cuckoo.create_cuckoo(new_position)
        return cuckoo


class CuckooFactory:
    @staticmethod
    def create_cuckoo(value):
        return CuckooThreshold(value)

def execute(hist, image_size):
    CuckooThreshold.image_size = image_size
    CuckooThreshold.hist = hist
    space_search = ThresholdSpaceSearch(UPPER_LIMIT, LOWER_LIMIT, THRESHOLDS, STEP_SCALE, CuckooFactory, ALPHA)
    seeker = Seeker(POPULATION_SIZE, DISCOVER_FACTOR, GENERATIONS, space_search)
    return seeker.search()

