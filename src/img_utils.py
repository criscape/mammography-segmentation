import cv2, numpy as np, pywt, scipy.signal, matplotlib.pyplot as plt


def load_image(path):
    '''Returns the image in grayscale'''
    return cv2.imread(path, cv2.IMREAD_GRAYSCALE)


def save_image(path, image):
    '''Save the image in the specified path'''
    cv2.imwrite(path, image)


def resize(image, percent):
    '''
    Change the size of the image in percent of the original.
    The interpolation method used is INTER_AREA
    '''
    width = int(image.shape[1] * percent / 100)
    height = int(image.shape[0] * percent / 100)
    return cv2.resize(image, (width, height), interpolation = cv2.INTER_AREA)


def artifact_remove(image, se_size):
    '''
    Apply Tophat and Otsus morphological operations to the image.
    se_size is the size of the structural element (shape -> ellipse)
    '''
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (se_size,se_size))
    
    tophat = cv2.morphologyEx(image, cv2.MORPH_TOPHAT, kernel)
    preotsus = tophat - image
    preotsus = cv2.GaussianBlur(preotsus, (5,5), 0)
    ret, otsus = cv2.threshold(preotsus, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)
    return image*otsus


def CV2_to_PYWT(image):
    '''Convert every pixel to float for more resolution for use pywt'''
    image = np.float32(image)
    image /= 255
    return image


def PYWT_to_CV2(image):
    '''Convert every pixel to int from 0 to 255 for use in cv2'''
    image *= 255
    image = np.uint8(image)
    return image


def enhancement(image, wav_family, dec_level):
    '''
    Apply 2D Discrete Wavelet Transform and Wiener Filter to enhance the image.
    Returns the recomposed approximation image with the Wiener Filter
    '''
    image = CV2_to_PYWT(image)
    wav_decomp = pywt.wavedec2(image, wav_family, level=dec_level)
    approx_wiener = scipy.signal.wiener(wav_decomp[0],3)
    wav_decomp[0] = approx_wiener # imagen de aproximación
    wav_recomp = pywt.waverec2(wav_decomp,wav_family)
    wav_recomp = PYWT_to_CV2(wav_recomp)
    return wav_recomp


def hist_reduction(image, wav_family, dec_level):
    '''
    Calculates the histogram from image and reduced it with 2D-DWT.
    Return the approximation signal from decomposition, equals to hist reduced
    '''
    hist = cv2.calcHist(images=[image], channels=[0], mask=None, histSize=[256], ranges=[0,256])
    descomposition = pywt.wavedec2(hist, wav_family, level=dec_level)
    return descomposition[0]


def graph_hist(hist, title, back_color):
    '''
    hist -> the histogram, no matters the length, that we want to graph
    title -> the title of the plot
    back_color -> plot's background color 
    '''
    fig, ax = plt.subplots(nrows=1, ncols=1)
    hist_len = len(hist)
    x_pos = np.arange(len(hist))
    y_pos = hist.tolist()
    y_pos = [y[0] for y in y_pos]
    color_range = [str(1 - x/hist_len) for x in range(0,hist_len)]
    ax.bar(x = x_pos, height = y_pos, align='center', alpha=0.5, color = color_range[::-1], width=1.4)
    plt.ylabel('Color intensity')
    plt.title(title)
    ax.set_facecolor(back_color)
    plt.show()


def threshold_image(image, class_to_gray_dict):
    '''
    Returns the thresholded image from a dictionary with the values,
    in gray, for each class in image. The dictionary key is the class.
    '''
    height, width = image.shape
    for i in range(height):
        for j in range(width):
            image[i][j] = class_to_gray_dict[image[i][j]]
    return image


def threshold_image_color(image, class_to_color_dict):
    '''
    Returns the thresholded image from a dictionary with the values,
    in RGB, for each class in image. The dictionary key is the class.
    '''
    image_rgb = cv2.cvtColor(image, cv2.COLOR_GRAY2RGB)
    height, width, channels = image_rgb.shape
    for i in range(height):
        for j in range(width):
            image_rgb[i][j] = class_to_color_dict[image[i][j]]
    return image_rgb
