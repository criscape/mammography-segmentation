from src import ga_utils as gu, img_utils as iu, ck_utils as ck

STRUCTURAL_ELEMENT_SIZE = 60
IMAGE_DECOMPOSITION_LEVEL = 3
HIST_DECOMPOSITION_LEVEL = 1
IMAGE_WAVELET_FAMILY = 'db1'
HIST_WAVELET_FAMILY = 'db1'
SOURCE_FOLDER = 'assets'
IMAGE_NAME = 'img'
IMAGE_EXT = '.jpg'

img = iu.load_image(SOURCE_FOLDER+'/'+IMAGE_NAME+IMAGE_EXT)

# Clean image and remove noise
img = iu.artifact_remove(img, STRUCTURAL_ELEMENT_SIZE)

# Enhance image to ease the segmentation process, the 2D-DWT used here is equals to dyadic wavelet descomposition
img = iu.enhancement(img, IMAGE_WAVELET_FAMILY, IMAGE_DECOMPOSITION_LEVEL)

# Save image to compare and analize results
iu.save_image(SOURCE_FOLDER+'/'+IMAGE_NAME+'_enhanced'+IMAGE_EXT, img)

# Mammography's histogram reduced from 0-255 levels to 256 / (2 ** dec_level)
reduced_hist = iu.hist_reduction(img, HIST_WAVELET_FAMILY, HIST_DECOMPOSITION_LEVEL)

#iu.graph_hist(reduced_hist, 'Histograma reducido con 2D DWT Wavelet (db1)', '#fab1a0')
#print(reduced_hist.tolist())
"""
A = gu.execute(reduced_hist, img.size)
#A = gu.remove_zero_chain(gu.random_chrom(gu.CHROMOSOME_LENGHT))
chrom_classes = gu.generate_class_original_size(A, 256, 1)
D = gu.class_to_color_dict(chrom_classes, 256, gu.COLORS)
img = iu.threshold_image_color(img, D)
iu.save_image('assets/img_segmented.jpg', img)
"""

ck.execute(reduced_hist, img.size)