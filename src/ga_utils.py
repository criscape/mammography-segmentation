import math, random, statistics


POPULATION_SIZE = 80 #It is recommended to be defined by: 150/2^j, where j is the resolution level
CHROMOSOME_LENGHT = 128 #Is given by the resolution level of the wavelet transform
GENERATIONS = 100
COMBINATION_PROBABILITY = 0.9
MUTATION_PROBABILITY = 0.001
SIMILARITY_PROBABILITY = 0.9
WIGHTING_CONSTANT = 1 # It is recommended to be defined by: 0.5 * 2^j, where j is the resolution level
K_TOURNAMENT = 2 #Amount of individuals to choose to compete


COLORS = {
	0: [180,134,140],
	1: [171,194,36],
	2: [86,34,147],
	3: [224,55,191],
	4: [6,105,34],
	5: [11,79,67],
	6: [47,10,174],
	7: [110,43,61],
	8: [74,104,3],
	9: [4,90,134],
	10: [92,105,63],
	11: [23,162,111],
	12: [41,194,254],
	13: [98, 92, 34],
	14: [124, 18, 135],
	15: [160, 63, 213],
	16: [90, 18, 181],
	17: [56, 89, 0],
	18: [189, 195, 199],
	19: [127, 140, 141],
	20: [64, 64, 122],
	21: [67, 33, 33],
	22: [26, 32, 67],
	23: [56, 232, 123],
	24: [67, 34, 86],
	25: [178, 32, 84],
	26: [71, 71, 46],
	27: [252, 76, 66],
	28: [211,89,45],
	29: [33, 140, 24],
	30: [110, 134, 151],
	31: [255, 121, 63],
	32: [233,87,21],
	33: [232, 192, 235],
	34: [78, 255, 126],
	35: [71, 74, 79],
	36: [250,245,87],
	37: [206,119,71],
	38: [82,199,45],
	39: [122,56,65]
}


def fit(ks, rho, hist, image_size):
	'''Fit[k] fitness function'''

	rho = 1
	return (rho*math.sqrt(dis(ks, hist, image_size))) + math.log(len(ks), 2)**2
	

def dis(ks, hist, image_size):
	
	'''Dis[k] discrepancy function'''
	return total_variance(hist, image_size) - between_variance(ks, hist, image_size)


def total_variance(hist, image_size):
	'''sigma**2(t) total variance of all classes'''
	average = intensity_expected_value(hist, image_size)
	return sum([ ((intensity - average)**2) * intensity_prob(intensity, hist, image_size) for intensity in range(len(hist)) ])


def between_variance(ks, hist, image_size):
	return sum([ ((class_mi(k, hist, image_size) - intensity_expected_value(hist, image_size))**2) * class_prob(k, hist, image_size) for k in ks ])


def class_prob(k, hist, image_size):
	'''P(i) class intensity probability for a class k'''
	return sum([intensity_prob(intensity, hist, image_size) for intensity in k])


def class_expected_value(k, hist, image_size):
	'''sigma(i) intensity expected value for a class k'''
	return sum([intensity * intensity_prob(intensity, hist, image_size) for intensity in k])


def class_mi(k, hist, image_size):
	'''m(i) '''
	prob = class_prob(k, hist, image_size)
	if prob == 0:
		return 0
	return class_expected_value(k, hist, image_size)/prob


def intensity_expected_value(hist, image_size):
	'''m intensity average in image'''
	return sum([ intensity * intensity_prob(intensity, hist, image_size) for intensity in range(len(hist)) ])


def intensity_prob(intensity, hist, image_size):
	'''p(n) intensity probability in image'''
	if image_size == 0:
		return 0
	return hist[intensity]/image_size

def vector_to_string(vector):
	return ''.join(vector)

def remove_zero_chain(chrom):
	'''Replace every 0 sequence to 1, except the first one 0'''
	index = 0
	chrom_len = len(chrom)
	new_chrom = chrom[index]
	while (index < chrom_len - 1):
		index = index + 1
		gen = chrom[index]
		prev_gen = chrom[index - 1]
		new_chrom = new_chrom + '1' if prev_gen == '0' and gen == '0' else new_chrom + gen
	return new_chrom


def random_chrom(chrom_length):
	'''Random string, with values in 1 or 0, with chrom_length size'''
	chrom_list = [str(random.randint(0,1)) for x in range(chrom_length)]
	chrom_list = remove_zero_chain(vector_to_string(chrom_list))
	return chrom_list


def population_generator(population_size, chrom_length):
	'''Random population from a specified population size'''
	return [random_chrom(chrom_length) for x in range(population_size)]


def class_generator(chrom):
	'''
	Each class in generated from the last zero (threshold), to the 
	current zero. 
	'''
	classes = []
	last_zero = 0
	zeros_index = [index for index, gen in enumerate(chrom) if gen == '0']
	for index in zeros_index:
		classes.append(list(range(last_zero, index)))
		last_zero = index
	classes.append(list(range(last_zero, len(chrom))))
	if len(classes[0]) == 0: del classes[0]
	return classes


def generate_class_original_size(chrom, hist_original_size, dec_level):
	'''
	Create a new chromosome with length = hist_original_size and multiply
	the thresholds by 2^dec_level, returns the class list with new size
	'''
	chrom_temp = ['1'] * hist_original_size
	zeros_index = [index*2**dec_level for index, gen in enumerate(chrom) if gen == '0']
	for index in zeros_index:
		chrom_temp[index] = '0'
	chrom_str = ''.join(chrom_temp)
	return class_generator(chrom_str)


def get_fitness_values(population, hist, image_size):

	rho = WIGHTING_CONSTANT
	fitness_values = [fit(class_generator(individual), rho, hist, image_size) for individual in population]

	return fitness_values

def get_best_fitness_individual(population, hist, image_size):
	fitness_values = get_fitness_values(population, hist, image_size)
	best_individual_index = fitness_values.index( min(fitness_values) ) #Pregunta para Jorge: ¿el Fit en este caso sería minimizar el valor de aptitud?
	best_individual = population[best_individual_index]
	
	return best_individual

def selection(population,hist, image_size):
	candidates = []
	while len(candidates) < len(population):
		random_individuals = random.sample(population, K_TOURNAMENT)
		best_individual = get_best_fitness_individual(random_individuals,  hist, image_size)
		candidates.append(best_individual)

	return candidates

def crossover(candidates):
	offspring = []
	randomized_candidates = random.sample(candidates, len(candidates))
	
	for i in range(0,len(candidates)-1, 2):
		descendientes = mating(randomized_candidates[i], randomized_candidates[i+1])
		offspring.extend(descendientes)

	return offspring

def mating(individualA, individualB):
	descendientes = []
	chromA = [x for x in individualA]
	chromB = [x for x in individualB]

	if random.random() < COMBINATION_PROBABILITY:
		midPoint = random.randint(0, len(individualA) - 1 )
		chromA[midPoint:], chromB[midPoint:] = chromB[midPoint:], chromA[midPoint:] 
	mutation(chromA)
	mutation(chromB)

	chromA = remove_zero_chain(vector_to_string(chromA))
	chromB = remove_zero_chain(vector_to_string(chromB))
		#Pregunta para Jorge: ¿La mutación solo se da cuando hay apareamiento?

	return [chromA,chromB]


def mutation(chrom):
	if random.random() < MUTATION_PROBABILITY:
		random_index = random.randint(0,len(chrom) - 1 )
		chrom[random_index] = '1' if chrom[random_index] == '0' else '0'


def class_to_dict(class_list, hist_original_size):
	'''
	Takes the class list and make a dictionary with 
	the respectively gray color intensity by class
	returns
	'''
	pixels_rule = {}
	for pixel in range(hist_original_size):
		pixel_color = -1
		k = 0
		while(pixel_color == -1 and k < len(class_list)):
			pixel_color = class_list[k][0] if pixel in class_list[k] else -1
			k += 1
		pixels_rule[pixel] = pixel_color
	return pixels_rule


def class_to_color_dict(class_list, hist_original_size, colors_dict):
	'''
	Takes the class list and make a dictionary with 
	the respectively RGB color intensity by class
	'''
	pixels_rule = {}
	for pixel in range(hist_original_size):
		pixel_color = -1
		k = 0
		while(pixel_color == -1 and k < len(class_list)):
			pixel_color = colors_dict[k] if pixel in class_list[k] else -1
			k += 1
		pixels_rule[pixel] = pixel_color
	return pixels_rule



def get_population_similarity(population):
	SYSTEM_BASE = 2
	decimals = [int(x, SYSTEM_BASE) for x in population]

	times_repeated = 0 

	if len(population) != len(set(population)): #if there are repeated individuals
		most_repeated_element = statistics.mode(population)
		times_repeated = population.count(most_repeated_element)

	similarity_porcentage = times_repeated / len(population)

	return similarity_porcentage
	


def execute(hist, image_size):
	initial_population = population_generator(POPULATION_SIZE, CHROMOSOME_LENGHT)
	population = initial_population
	i = 0
	#or get_population_similarity(population) < SIMILARITY_PROBABILITY 
	
	while i < GENERATIONS:
		print("Generation: ", i)
		candidates = selection(population, hist, image_size)
		population = crossover(candidates)
		i += 1
		
	
	super_individual = get_best_fitness_individual(population, hist, image_size)
	print(super_individual)
	return super_individual
